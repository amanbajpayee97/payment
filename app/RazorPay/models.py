from datetime import datetime

from marshmallow_jsonapi import Schema, fields
from sqlalchemy import ForeignKey
from app import db


class OrderModel(db.Model):
    __tablename__ = "Order_model"

    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    order_id = db.Column(db.String(128))
    entity = db.Column(db.String(128))
    customer_name=  db.Column(db.String(128))
    #customer_id = db.Column(db.Integer, ForeignKey('companies_profile.id'))
    amount = db.Column(db.Integer())
    amount_paid = db.Column(db.Integer())
    amount_due = db.Column(db.Integer())
    currency = db.Column(db.String(128))
    receipt = db.Column(db.String(128))
    offer_id = db.Column(db.String(128))
    status = db.Column(db.String(128))
    attempts = db.Column(db.Integer())
    notes = db.Column(db.String(128))
    created_at = db.Column(db.DateTime, default = datetime.utcnow)
    razorpay_payment_id = db.Column(db.String(128))
    razorpay_signature = db.Column(db.String(128))

class Order_Schema(Schema):
    class Meta:
        type_ = 'order'
        self_view = 'order_detail'
        self_view_kwargs = {'id': '<id>'}
        self_view_many = 'order_list'

    id = fields.Int(load_only=True)
    order_id = fields.Str()
    customer_id = fields.Int()
    entity = fields.Str()
    amount_due = fields.Int()
    amount_paid = fields.Int()
    amount = fields.Int()
    currency = fields.Str()
    receipt = fields.Str()
    offer_id = fields.Str()
    status = fields.Str()
    attempts = fields.Int()
    created_at = fields.DateTime()
    payment_capture = fields.Bool(default=False)
    #notes = {'Shipping address': 'Bommanahalli, Bangalore'}
    notes = fields.Str()
    razorpay_payment_id = fields.Str()
    #razorpay_order_id = fields.Str()
    razorpay_signature = fields.Str()

