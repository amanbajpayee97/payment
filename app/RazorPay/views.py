from flask import g, request
from flask_rest_jsonapi import ResourceList, ResourceDetail

from app.RazorPay.models import OrderModel, Order_Schema
from lib.jwt_utils import login_required

from app import db
import razorpay
import json

class Order_Detail(ResourceDetail):
    def before_update_object(self, obj, data, view_kwargs):
        razorpay_order_id = data["order_id"]
        razorpay_payment_id = data["razorpay_payment_id"]
        razorpay_signature = data["razorpay_signature"]

        import hmac
        import hashlib

        # api_key = "rzp_test_WVodeR1LaHJKPq"
        API_SECRET = "XwUnls4kjkGblDr21gqRtNsE"

        message = bytes(razorpay_order_id + "|" + razorpay_payment_id, 'utf-8')
        generated_signature = hmac.new(
            str(API_SECRET),
            msg=message,
            digestmod=hashlib.sha256
        ).hexdigest().upper()
        print(generated_signature)
        print(razorpay_signature)

        if (generated_signature == razorpay_signature):
            razorpay_client = razorpay.Client(auth=("rzp_test_WVodeR1LaHJKPq", "XwUnls4kjkGblDr21gqRtNsE"))
            params_dict = dict()
            params_dict['razorpay_order_id'] = data["order_id"]
            params_dict['razorpay_payment_id'] = data["razorpay_payment_id"]
            params_dict['razorpay_signature'] = data["razorpay_signature"]

            razorpay_client.utility.verify_payment_signature(params_dict)

            data['amount_paid'] = data['amount']
            data['amount_due'] = 0
            data['status'] = "Payment Successful"

        else:
            data['status'] = "Payment Failure"

        return data

    # def after_get_object(self, obj, view_kwargs):
    #     url = "https://www.example.com/payment/success/"
    #     src = "https://checkout.razorpay.com/v1/checkout.js"
    #     key = "rzp_test_WVodeR1LaHJKPq"
    #     desc = ""
    #     name = ""
    #     email = ""
    #     form ='<form action='+url+' method="POST"><script    src='+src+'   data-key='+key+'   data-amount='+obj['amount']+'   data-currency='+obj['currency']+'   data-order_id='+obj['order_id']+'    data-buttontext="Pay with Razorpay"    data-name="NOSTOPS"    data-description='+desc+'    data-image="https://example.com/your_logo.jpg"    data-prefill.name="+name+"    data-prefill.email="+email+"    data-theme.color="#F37254"></script><input type="hidden" custom="Hidden Element" name="hidden"></form>'


    #decorators = (login_required,)
    methods = ['GET', 'POST', 'PATCH']
    schema = Order_Schema
    data_layer = {'session': db.session,
                  'model': OrderModel,
                  'methods':{'before_update_object': before_update_object} } #'after_get_object':after_get_object}}


class Order_List(ResourceList):
    def  before_create_object(self,data,view_kwargs):
        razorpay_client = razorpay.Client(auth=("rzp_test_WVodeR1LaHJKPq", "XwUnls4kjkGblDr21gqRtNsE"))

        response = razorpay_client.order.create(data=data)

        data['order_id'] = response['id']
        data['entity'] = response['entity']
        data['amount_paid'] = response['amount_paid']
        data['amount_due'] = response['amount_due']
        data['amount'] = response['amount']
        data['currency'] = response['currency']
        data['receipt'] = response['receipt']
        data['status'] = response['status']
        data['attempts'] = response['attempts']
        data['notes'] = json.dumps(response['notes'])

        return data

    #decorators = (login_required,)
    methods = ['GET', 'POST', 'PATCH']
    schema = Order_Schema
    data_layer = {'session': db.session,
                  'model': OrderModel,
                  'methods':{'before_create_object': before_create_object}
                  }




