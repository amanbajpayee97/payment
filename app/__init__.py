import logging

from flask import Flask
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_rest_jsonapi import Api
from flask_sqlalchemy import SQLAlchemy

import lib.log as log
from config import config, APP_NAME

Logger = logging.getLogger(APP_NAME)
db = SQLAlchemy()

ma = Marshmallow()
api = Api()


def initialize_db(app):
    db.init_app(app)
    ma.init_app(app)
    migrate = Migrate(app, db)


def create_app(config_name):
    app = Flask(__name__)

    CORS(app, resources={r"/*": {"origins": "*"}})
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)
    log.setup_logging(config[config_name])

    initialize_db(app)

    from app.RazorPay.views import Order_List
    api.route(Order_List, 'order_list', '/orders')

    from app.RazorPay.views import Order_Detail
    api.route(Order_Detail, 'order_detail', '/orders/<int:id>')  # ,'/orders/<int:id>/get_checkout_form'

    api.init_app(app)

    return app
